const { app, BrowserWindow } = require('electron')

var debug = false;


global.sharedObject = {
  debug: debug
}

/////////////// START SERVER /////////////////

var tuio = (function() {
    var dgram = require("dgram"),
    WebSocketServer = require('ws').Server,
    server = null,
    udpSocket = null,
    webSocketClients = [],

    init = function(params) {
        udpSocket = dgram.createSocket("udp4");
        udpSocket.on("listening", onSocketListening);
        udpSocket.bind(params.oscPort, params.oscHost);
        udpSocket.on("message", function(msg) {
            webSocketClients.forEach(function(webSocket) {
                if ( webSocket.readyState === webSocket.OPEN ) {
                    webSocket.send(msg, function onWebSocketSendError(error) {
                        if (typeof error !== "undefined") {
                            console.log(error);
                        }
                    });   
                }
            });
        }),
        
        server = new WebSocketServer({
            port: params.socketPort
        });
        
        server.on("connection", onSocketConnection);
    },

    onSocketListening = function() {
        var address = udpSocket.address();
        console.log("TuioServer listening on: " + address.address + ":" + address.port);
    },

    onSocketConnection = function(webSocket) {
        webSocketClients.push(webSocket);
        console.log("Websocket client connected");
        webSocket.on("close", function() {
            var indexOf = webSocketClients.indexOf(webSocket);
            webSocketClients.splice(indexOf, 1);
            console.log("Websocket client disconnected");
        });
    };

    return {
        init: init
    };
}());

tuio.init({
  oscPort: 3333,
  oscHost: "0.0.0.0",
  socketPort: 8080
});


//////////////// END START SERVER //////////////

// Gardez une reference globale de l'objet window, si vous ne le faites pas, la fenetre sera
// fermee automatiquement quand l'objet JavaScript sera garbage collected.
let win

function createWindow () {
  // Créer le browser window.
  win = new BrowserWindow({
    width: 1920,
    height: 1080,
    titleBarStyle: "hidden",
    frame:debug,
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true
    }
  });

  // and load the index.html of the app.
  win.loadFile('app/canvas.html')

  // Ouvre les DevTools.
  if (debug) win.webContents.openDevTools();
  else win.setFullScreen(true);

  // Émit lorsque la fenêtre est fermée.
  win.on('closed', () => {
    // Dé-référence l'objet window , normalement, vous stockeriez les fenêtres
    // dans un tableau si votre application supporte le multi-fenêtre. C'est le moment
    // où vous devez supprimer l'élément correspondant.
    win = null
  })
}

// Cette méthode sera appelée quant Electron aura fini
// de s'initialiser et sera prêt à créer des fenêtres de navigation.
// Certaines APIs peuvent être utilisées uniquement quand cet événement est émit.
app.on('ready', createWindow)

// Quitte l'application quand toutes les fenêtres sont fermées.
app.on('window-all-closed', () => {
  // Sur macOS, il est commun pour une application et leur barre de menu
  // de rester active tant que l'utilisateur ne quitte pas explicitement avec Cmd + Q
    app.quit();
})

app.on('activate', () => {
  // Sur macOS, il est commun de re-créer une fenêtre de l'application quand
  // l'icône du dock est cliquée et qu'il n'y a pas d'autres fenêtres d'ouvertes.
  if (win === null) {
    createWindow()
  }
})
