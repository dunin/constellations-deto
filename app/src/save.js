const fs = require('fs');
const fontSize = 36;
const font = fontSize+'px undefined';
const { exec } = require('child_process');
const rsyncCon = [
	// {id:"dunin",pass:"truc",path:"ip:~/path"}
];

var stratT;
var zoom = 0.5;
var zoomCalc = zoom;
var cielWidth = 1920*2;
var cielHeight = 1080*2;


var ciel = (fs.existsSync("app/res/ciel/ciel.json"))? JSON.parse(fs.readFileSync('app/res/ciel/ciel.json', 'utf8')) : [];
var cielCanvas = new OffscreenCanvas(cielWidth, cielHeight);
var cielCtx = cielCanvas.getContext("2d");
// cielCtx.fillStyle = "black";
// cielCtx.fillRect(0,0,cielWidth,cielHeight);
// var cielDebugCanvas = new OffscreenCanvas(cielWidth, cielHeight);
// var cielDebugCtx = cielDebugCanvas.getContext("2d");
// cielDebugCtx.fillStyle = "red";
var ready=false;

var promises = [];
for (var i = 0;i<ciel.length;i++){
	promises.push(drawConst(ciel[i]));
}

Promise.all(promises).then(function(){
	var dicos = parseMots();
	// on cherche si il y a des étoiles de disponibles
    etoiles = fs.readdirSync('app/res/etoiles/');
	postMessage({type:"jsonLoad",dico:dicos.dico,dicoUsed:dicos.dicoUsed,etoiles:etoiles,stars:ciel.reduce((acc,el)=>{return el.stars.concat(acc)},[])});
}) 


onmessage = function(e) {
	console.log(e.data);
	if(e.data.constellation) saveConstellation(e.data.nom,e.data.constellation,e.data.screenW);
	 
}

function parseMots (lg) {
	// lecture du dictionnaire
	dico = fs.readFileSync('app/res/dictionnaire', 'utf8')
    dico = dico.split(/\s*\n\s*/m);
    dico = dico.filter(function(el) {
        if(el.indexOf("//")==0) return false;
        return el;
    });

    // on écarte les mots déjà utilisés
    var dicoUsed = [],id;

    for (var i=0;i<ciel.length;i++) {
    	id = dico.indexOf(ciel[i].nom);
    	if(id!=-1) dicoUsed.push( dico.splice(id,1)[0] );
    }


    return {dico:dico,dicoUsed:dicoUsed};
}

function setCtxStyles(ctx) {
	ctx.strokeStyle = ctx.fillStyle = "white";
    ctx.lineWidth = 2;
    ctx.font = font;
    ctx.textBaseline = "top";
}

function saveConstellation(nom, constellation,screenW) {
	zoomCalc = cielWidth/screenW*zoom;
	startT = Date.now();

	// détecter la bonne taille
	var startX=10000000000, endX=0, startY =10000000000, endY = 0;

	for (var c of constellation) {
		console.log("c",c)
		if (startX > c.x+c.offsetX) startX = c.x+c.offsetX;
		if (endX < c.x+c.offsetX+c.img.width) endX = c.x+c.offsetX+c.img.width;
		if (startY > c.y+c.offsetY) startY = c.y+c.offsetY;
		if (endY < c.y+c.offsetY+c.img.height) endY = c.y+c.offsetY+c.img.height;
	}

	console.log("startX",startX,"startY",startY,"endX",endX,"endY",endY);
	var width = Math.round((endX-startX)*zoomCalc);
	var height = Math.round((endY-startY)*zoomCalc)
	var canvas = new OffscreenCanvas(width, height);
	var ctx = canvas.getContext("2d");

	// draw links
	setCtxStyles(ctx);
                    
	for (var c of constellation) {
		c.x -= startX;
		c.y -= startY;
		if (c.linkTo) {
			c.linkTo.x -= startX;
			c.linkTo.y -= startY;
            ctx.beginPath();
            ctx.moveTo(c.x*zoomCalc,c.y*zoomCalc);
            ctx.lineTo(c.linkTo.x*zoomCalc,c.linkTo.y*zoomCalc);
            ctx.stroke();
		}
	}

	// draw stars
	for (var c of constellation) {
		ctx.drawImage(c.img,(c.x+c.offsetX)*zoomCalc,(c.y+c.offsetY)*zoomCalc,c.img.width*zoomCalc,c.img.height*zoomCalc);
	}

	// strip black
	var imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
	var data = imgData.data;
	var mask = [];
	var w4 = canvas.width*4;

	for(var i=0;i<data.length;i+=4) {
		if(i<w4) mask.push([]);
		if(data[i+3]>0 && data[i] !==255  && data[i+1] !==255 && data[i+2] !=255 ) {
			data[i+3]=data[i];
			data[i] = data[i+1] = data[i+2] = 255;
		}
		mask[(i%w4)/4][Math.floor(i/w4)] = (data[i+3] > 0);
	}

	// ctx.putImageData(imgData, 0, 0);


	// adding name
	let textWidth= ctx.measureText(nom).width;
	console.log("textWidth",textWidth,"fontSize",fontSize);
	let steps = 10;
	let stepsY = Math.floor((height-fontSize)/steps);
	let bestProgress = 0;
	let bestProgressY, bestProgressX,progressLeft,progressRight,x,endLeft,endRight,j;
	let offsetXText = bestProgressOffset = fontSize*0.5;
	let progressLeftOffset, progressRightOffset;

	for (var y=0;y<height-fontSize;y+=stepsY) {
		progressLeft = progressRight = x = endLeft = endRight = 0;
		progressLeftOffset = progressRightOffset = offsetXText;

		while(!endLeft || !endRight) {
			for(j=0;j<fontSize;j++) {
				if(!endLeft && (!mask[x] || mask[x][y+j])) {
					endLeft = true;
					progressLeft = x;
					if(j<fontSize/2) progressLeftOffset = offsetXText*1.5; // on touche par le haut du texte
				}
				if(!endRight && (!mask[width-x-1]||mask[width-x-1][y+j])) {
					endRight = true;
					progressRight = x;
					if(j<fontSize/2) progressRightOffset = offsetXText*1.5; // on touche par le haut du texte
				}
			}
			x++;
		}

		if(progressRight>bestProgress) {
			bestProgress = progressRight;
			bestProgressY = y;
			bestProgressX = width - x + progressLeftOffset;
			bestProgressOffset = progressLeftOffset; 
		}

		if(progressLeft>bestProgress) {
			bestProgress = progressLeft;
			bestProgressY = y;
			bestProgressX = x - textWidth - progressRightOffset;
			bestProgressOffset = progressRightOffset; 
		}
	}

	// we create a new canvas to put the name and the constellation
	let newWidth = width + Math.max(textWidth + bestProgressOffset - bestProgress,0);
	canvas = new OffscreenCanvas(newWidth, height);
	ctx = canvas.getContext("2d");
	setCtxStyles(ctx);
	var newOffsetX = (bestProgressX<0)? -bestProgressX : 0;
	ctx.putImageData(imgData, newOffsetX, 0);
	ctx.fillText(nom,(bestProgressX<0)? 0 : bestProgressX, bestProgressY);

	// we complete the mask
	while (mask.length<newWidth) {
		var newCol = new Array(mask[0].length);
		newCol.fill(false);
		if(bestProgressX<0) mask.unshift(newCol);
		else mask.push(newCol);
	}
	bestProgressX = (bestProgressX<0)? 0 : Math.floor(bestProgressX);
	console.log("bestProgressX",bestProgressX,"mask.length",mask.length,"canvas.width",canvas.width,"bestProgressX+textWidth",bestProgressX+textWidth);
	console.log("bestProgressY",bestProgressY,"mask[0].length",mask[0].length,"mask[mask.length-1].length",mask[mask.length-1].length,"canvas.height",canvas.height,"bestProgressY+fontSize",bestProgressY+fontSize);
	for(var l=bestProgressX;l<bestProgressX+textWidth;l++) {
		for (var m=bestProgressY;m<=bestProgressY+fontSize;m++) {
			mask[l][m] = true;
		}
	}


	// test mask
	// ctx.fillStyle = "rgba(255,0,0,0.5)";
	// for (var n=0;n<mask.length;n++) {
	// 	for (var o=0;o<mask[0].length;o++) {
	// 		if(mask[n][o]) ctx.fillRect(n-0.5,o-0.5,1,1);
	// 	}
	// }
	

	var coor = getFreePlace(canvas.width,canvas.height,mask);

	// save image
	var name = convertName(nom);
	var num = 0;
	while (fs.existsSync("app/res/constellations/"+name+".png")) {
		num++;
		name = convertName(nom)+"_"+((num<10)?"0"+num:num);
	}


	// memorize one star to flash it
	let starsMem = [];

	for (var c of constellation) {
		starsMem.push({x:coor.x+(c.x+c.offsetX+newOffsetX)*zoomCalc,y:coor.y+(c.y+c.offsetY)*zoomCalc,w:c.img.width*zoomCalc,h:c.img.height*zoomCalc});
	}

	constellation = {x:coor.x,y:coor.y,width:canvas.width,height:canvas.height,nom:nom,img:name+".png",stars:starsMem};

	


	saveCanvas(canvas, "app/res/constellations/"+name+".png")
	.then( ()=>{
		ciel.push(constellation);
		fs.writeFileSync('app/res/ciel/ciel.json', JSON.stringify(ciel))
		drawConst(constellation).then(function(){
			// cielCtx.drawImage(cielDebugCanvas,0,0);
			saveCanvas(cielCanvas, "app/res/ciel/ciel.png",function(){})
				.then(endSave);
		});
	})

	
}

function getFreePlace(width, height,mask) {
	var nbTentatives = 1000;
	var nbTentativesToDelete = 20;

	var rX,rY,tentatives=0,test;

	do {
		rX = Math.floor(Math.random()*(cielWidth-width));
		rY = Math.floor(Math.random()*(cielHeight-height));
		tentatives++;
	} while (!(test=testFree(rX,rY,width,height,mask))&&tentatives<nbTentatives);

	if(tentatives==nbTentatives && !test) {
		// on n'a pas trouvé de placement libre, on va devoir supprimer des constellations
		var toDelete = null, toDeleteTest, tX, tY;
		for (var i=0;i<nbTentativesToDelete;i++) {
			tX = Math.floor(Math.random()*(cielWidth-width));
			tY = Math.floor(Math.random()*(cielHeight-height));
			toDeleteTest = [];
			for (var j=0;j<ciel.length;j++) {
				if (checkCollision(tX,tY,width,height,mask,ciel[j])) toDeleteTest.push(ciel[j]);
			}

			if (toDelete===null || toDelete.length>toDeleteTest.length) {
				toDelete = toDeleteTest;
				rX = tX;
				rY = tY;
			}
		}

		console.log ("Suppression de constellations pour s'installer en "+rX+" , "+rY);
		for (var i=0;i<toDelete.length;i++) {
			removeConst(toDelete[i]);
		}
		
	}

	return {x:rX,y:rY};
}

function testFree(x,y,w,h,mask) {
	// console.log("x",x,"y",y,"w",w,"h",h);
	for (var i=0;i<ciel.length;i++) {
		if (checkCollision(x,y,w,h,mask,ciel[i])) return false;
	}
	return true;
}

function checkCollision(x,y,w,h,mask,constl) {
	var imgData, offX, offY, width, height, startX, startY;
	if(x+w > constl.x && x < constl.x+constl.width
		&& y+h > constl.y && y < constl.y+constl.height) {
		// console.log ("AABB Collision with "+constl.nom);
		// AABB collision, il faut essayer le mask
		if(x>constl.x) {
			startX = x;
			offX = 0;
			width = Math.min(w, constl.width - (x-constl.x));
		} else {
			startX = constl.x;
			offX = constl.x -x;
			width = Math.min ( constl.width, w-(constl.x-x) );
		}

		if(y>constl.y) {
			startY = y;
			offY = 0;
			height = Math.min(h, constl.height - (y-constl.y));
		} else {
			startY = constl.y;
			offY = constl.y -y;
			height = Math.min ( constl.height, h-(constl.y-y) );
		}

		imgData = (cielCtx.getImageData(startX,startY,width,height)).data;
		// console.log(imgData[0],imgData[1],imgData[2],imgData[3])

		for (var j=0;j<width;j++) {
			for (var k=0;k<height;k++) {
				if (mask[offX+j][offY+k] 
					&& imgData[(k*width+j)*4+3]>0) {
					// cielDebugCtx.fillStyle = "red";
					// cielDebugCtx.fillRect(startX+j,startY+k,1,1);
					return true;
				}
			}
		}
	} 
	return false;
}

function convertName(name) {
	return name.replace(/^(le|la|l'|l’|les)\s?/i,"").normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
}

function saveCanvas (canvas, path) {
	return new Promise(resolve => {
		canvas.convertToBlob({ type: "image/png" })
			.then(blob=>{
				var arrayBuffer, buf;
			    var fileReader = new FileReader();
			    fileReader.onload = function() {
			        arrayBuffer = this.result;
			        buf  = new Uint8Array(arrayBuffer);
			        fs.writeFileSync(path, buf);
			        resolve();
			    };
			    fileReader.readAsArrayBuffer(blob);
			})
		
  });
	
}

function endSave(){
	sendCiel();
	setTimeout(function(){
		postMessage({type:"saved",stars:ciel.reduce((acc,el)=>{return el.stars.concat(acc)},[])})
	}, Math.max(1500-(Date.now()-startT), 0 ) );
}

function drawConst(constellation) {
	// var imgBlob = new Blob ( fs.readFileSync('app/res/constellations/'+constellation.img), {type : 'image/png'} );
	/*var imgBlob = new Blob ( new Uint8Array(fs.readFileSync('app/res/constellations/'+constellation.img,'base64')), {type : 'image/png'} );
	console.log(imgBlob);
	createImageBitmap(imgBlob).then(function(img){
		console.log("img",img);
  		cielCtx.drawImage(img,constellation.x,constellation.y);
  	}).catch((e)=>console.log(e,e.message));*/
	// fetch('res/constellations/croissant.png')
	return new Promise(resolve => {
		fetch('../res/constellations/'+constellation.img).then( res => {
			res.blob().then(blob=>{
				createImageBitmap(blob).then(img=>{
					cielCtx.drawImage(img,constellation.x,constellation.y);
					console.log("constellation drawn");
					resolve();
				})
			})
		})
	
	})  
}

function removeConst(constl) {
	return new Promise(resolve => {
		fetch('../res/constellations/'+constl.img).then( res => {
			res.blob().then(blob=>{
				createImageBitmap(blob).then(img=>{
					// on crée un mask pour éviter les transparences
					var maskCanvas = new OffscreenCanvas(img.width, img.height);
					var maskCtx = maskCanvas.getContext("2d");
					maskCtx.drawImage(img,0,0);
					var maskImgData = maskCtx.getImageData(0,0,img.width,img.height);
					var maskData = maskImgData.data;
					for(var i=3; i < maskData.length;i+=4){
						if(maskData[i]!==0) maskData[i] = 255;
					}

					maskCtx.putImageData(maskImgData,0,0);

					cielCtx.globalCompositeOperation = "destination-out";
					cielCtx.drawImage(maskCanvas,constl.x,constl.y);
					console.log("constellation remove",JSON.stringify(constl));
					ciel.splice(ciel.indexOf(constl),1);
					cielCtx.globalCompositeOperation = "source-over";
					resolve();
				})
			})
		})
	
	}) 
	
}

function sendCiel() {
	console.log ("send ciel");
	rsyncCon.forEach((el)=>{
		exec('rsync --rsh="sshpass -p '+el.pass+' ssh -l '+el.id+'" -az ~/Desktop/constellations-deto/app/res/ciel/ '+el.path,
		(error,stdout,stderr)=>{
			if(error) console.error("rsync error",error);
			if (stdout) console.log("rsync : ",stdout);
			if(stderr) console.log("rsync error",stderr)
		});
	});
}
