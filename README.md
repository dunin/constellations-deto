# Jeu Constellations pour festival Détonation

installation:

avoir node et npm d'installés, avoir téléchargé TUIO ou TUIO_simulator

puis: 

```
npm install

lancer TUIO ou TUIO_simulator

npm start
```

les constellations sont stockées dans app/res/constellations, on peut regarder le ciel dans app/res/ciel.png (sur fond transparent)